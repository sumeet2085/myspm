// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
        name: "mobilypackages",
        platforms: [.iOS(.v12)],
        products: [
            .library(name: "mobilypackages", targets: ["mobilypackages"]),
            .library(name: "MobilyAppClipPackage", targets: ["MobilyAppClipPackage"]),
            .library(name: "mobilyIntentpackage", targets: ["mobilyIntentpackage"]),
        ],
        dependencies: [
            .package(url: "https://github.com/algolia/instantsearch-ios", from: "7.0.0"),
            .package(url: "https://github.com/apple/swift-algorithms.git", from: "1.0.0"),
            .package(url: "https://github.com/danielgindi/Charts.git", from: "4.0.0"),
            .package(url: "https://github.com/hackiftekhar/IQKeyboardManager.git", from: "6.5.0"),
            .package(url: "https://github.com/Alamofire/Alamofire.git", from: "5.6.1"),
            .package(url: "https://github.com/jrendel/SwiftKeychainWrapper.git", from: "4.0.0"),
            .package(url: "https://github.com/firebase/firebase-ios-sdk.git", from: "10.4.0"),
            .package(url: "https://github.com/WenchaoD/FSCalendar.git", from: "2.8.3"),
            .package(url: "https://github.com/airbnb/lottie-spm.git", from: "4.2.0"),
            .package(url: "https://github.com/rechsteiner/Parchment.git", from: "3.2.1"),
            .package(url: "https://github.com/TakeScoop/SwiftyRSA.git", from: "1.0.0"),
            
            .package(url: "https://github.com/onevcat/Kingfisher.git", from: "7.0.0"),
           
        ],
        targets: [
            .target(
                name: "mobilypackages",
                dependencies: [
                    .product(name: "Alamofire", package: "Alamofire"),
                    .product(name: "Algorithms", package: "swift-algorithms"),
                    .product(name: "Charts", package: "Charts"),
                    .product(name: "IQKeyboardManagerSwift", package: "IQKeyboardManager"),
                    .product(name: "SwiftKeychainWrapper", package: "SwiftKeychainWrapper"),
                    .product(name: "FirebaseAnalyticsWithoutAdIdSupport", package: "firebase-ios-sdk"),
                    .product(name: "FirebaseCrashlytics", package: "firebase-ios-sdk"),
                    .product(name: "FirebaseDynamicLinks", package: "firebase-ios-sdk"),
                    .product(name: "FirebaseRemoteConfig", package: "firebase-ios-sdk"),
                    .product(name: "FirebaseMessaging", package: "firebase-ios-sdk"),
                    .product(name: "FSCalendar", package: "FSCalendar"),
                    .product(name: "Lottie", package: "lottie-spm"),
                    .product(name: "Parchment", package: "Parchment"),
                    .product(name: "SwiftyRSA", package: "SwiftyRSA"),
                    .product(name: "InstantSearch", package: "instantsearch-ios"),
                    .product(name: "Kingfisher", package: "Kingfisher"),
                    .target(name: "uSDK"),
                    .target(name: "ServicesKIT"),
                    .target(name: "Gateway"),
                ]),
            .target(
                name: "MobilyAppClipPackage",
                dependencies: [
                    .product(name: "Alamofire", package: "Alamofire"),
                    .product(name: "FirebaseAnalyticsWithoutAdIdSupport", package: "firebase-ios-sdk"),
                    .product(name: "FirebaseRemoteConfig", package: "firebase-ios-sdk"),
                ],
                path: "Sources/MobilyAppClipPackage"  // Specify the correct path here
            ),
            .target(
                name: "mobilyIntentpackage",
                dependencies: [
                    .product(name: "Alamofire", package: "Alamofire"),
                    .product(name: "SwiftKeychainWrapper", package: "SwiftKeychainWrapper"),
                ],
                path: "Sources/mobilyIntentpackage"  // Specify the correct path here
             ),
            .binaryTarget(
                       name: "uSDK",
                       path: "Framework/uSDK.xcframework.zip"
                   ),
            .binaryTarget(
                       name: "ServicesKIT",
                       path: "Framework/ServicesKIT.xcframework.zip"
                   ),
            .binaryTarget(
                       name: "Gateway",
                       path: "Framework/Gateway.xcframework.zip"
                   ),
            .testTarget(
                name: "mobilypackagesTests",
                dependencies: ["mobilypackages"]),
        ]
    )
